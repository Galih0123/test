package com.example.test4

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

class MainActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<SwipeRefreshLayout>(R.id.swiper).setOnRefreshListener(this)
        setContent {
            simpleCompose()

        }
    }


    @Composable
    fun simpleCompose() {
        Text("Helo ya")
    }

    @Preview
    @Composable
    fun simpleCompose2() {
        Text("Helo ya")
    }

    override fun onRefresh() {
        TODO("Not yet implemented")
    }
}